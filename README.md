# play



## INFO 

This is repository for explaining database related concepts easily.

Taken from the Designing Data Intensive Systems the bash scripts tries to describe key value database concepts gradually.
You can use the bash scripts with codes:
- db_set 123456 '{"name":"London","attractions":["Big Ben","London Eye"]}'
- db_set 42 '{"name":"San Francisco","attractions":["Golden Gate Bridge"]}'
- db_set 42 '{"name":"San Francisco","attractions":["Exploratorium"]}'
- db_get 42



