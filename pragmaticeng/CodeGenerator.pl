#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

my $filepath;
our $Field = 'F';	# option variable with default value
our $MessageDef = 'M'; # option variable with default value
our $EndMessage = 'E'; # option variable with default value
our $Comment = '#'; # option variable with default value
my $TEMPLATE_COMMENT = '-'; #Global comment variable to detect template comments, followed after comment


my %variables = ( int => "integer",
                  char => "char",
                );

GetOptions ("filepath|fp=s" => \$filepath,  # string 
            "field|fd:s"   => \$Field,      # string
            "messagedef|md:s"  => \$MessageDef, # string
            "comment|cmt:s"  => \$Comment, # string
            "endmessage|ed:s" => \$EndMessage)   # string
or die("Error in command line arguments\n");
die("ERROR: filepath path must be specified.") unless defined $filepath;

our %Commands = ( $Field => 'field',
                 $MessageDef => 'start',
                 $Comment => 'comment',
                 $EndMessage => 'end'
               );
#my $filename = '/home/aeg/Desktop/pragmaticeng/play/pragmaticeng/AddProduct.template';


open(FH, '<', $filepath) or die $!;
print("File $filepath opened successfully!\n");
my $messagedef="";
while(<FH>){
   chomp;
   #Regular Expression to extract the message template format, first char type and other as remaining to further process based on the type
   if(/\A(.)[^$TEMPLATE_COMMENT](.*)/)
   {
      my($type, $remainingstr) = ($1, $2);
      if($type eq $Field)
      {
         # print "$remainingstr \n";
         my($varname, $vartype, $length) = ($remainingstr =~ /([a-z]*)\s+([a-z]+)\[?(\d+)?\]?/);
         # print "Variable name is: $varname, variable type is: $vartype \n";
         # print "length is: $length \n" if $length;
         # generateC($Commands{$type}, $varname, $variables{$vartype}, $length);
         generatePascal($Commands{$type}, $varname, $variables{$vartype}, $length);
         
      }
      else
      {
         if($type eq $MessageDef)
         {
            $messagedef=$remainingstr;
         }
         # generateC($Commands{$type}, $remainingstr);
         generatePascal($Commands{$type}, $remainingstr);
      }
      # print "Remainder strig is: $remainingstr \n";
   }
   else {
      if(/\A($EndMessage)/)
      {
         # generateC($Commands{$EndMessage}, $messagedef);
         generatePascal($Commands{$EndMessage}, $messagedef);
      }

   }
}
close(FH);

#Texti okurken mi generate etmek anlamlı template okuyup ondan mı generate etmek mantıklı?
sub generatePython {
my $CHAR="char";


}

sub generateC {
my $CHAR="char";
my $INT="int";
my %variables = ( "integer" => $INT,
                  "char" => $CHAR,
                );
my %structdefinition = ( start => "typedef struct {\n",
                         comment => "/* %s */\n", #comment block
                         end => "} %s;\n" #message definition
);
my %fielddefinition = ( array => "%s   %s[%d];\n", #type, variable name, length  
                        $INT => "%s     %s;\n" #type, variable name
);
my ($type, $data) = (@_);
if($type eq $Commands{$Field})
{
   my ($other, $varname, $vartype, $length) = (@_);
   my $curvartype;

   if($length)
   {
      $curvartype = "array";
      printf $fielddefinition{$curvartype},$vartype,$varname,$length;

   }
   else 
   {
      $curvartype = $variables{$vartype};
      printf $fielddefinition{$curvartype},$curvartype,$varname;
   } 
}
else
{
   printf $structdefinition{$type},$data;
}

}

sub generatePascal {
my $CHAR="char";
my $INT="LongInt";
my %variables = ( "integer" => $INT,
                  "char" => $CHAR,
                );
my %structdefinition = ( start => "%s = packed record \n",
                         comment => "{ %s }\n", #comment block,
                         end => "end;\n"
);
my %fielddefinition = ( array => "   %s:   array[0..%d] of %s;\n", #variable name,length-1,type,   
                        $INT => "   %s:     %s;\n" #variable name, type
);
my ($type, $data) = (@_);
if($type eq $Commands{$Field})
{
   my ($other, $varname, $vartype, $length) = (@_);
   my $curvartype;

   if($length)
   {
      $curvartype = "array";
      printf $fielddefinition{$curvartype},$varname,$length-1,$vartype;

   }
   else 
   {
      $curvartype = $variables{$vartype};
      printf $fielddefinition{$curvartype},$varname,$curvartype;
   } 
}
else
{
   printf $structdefinition{$type},$data;
}  
}