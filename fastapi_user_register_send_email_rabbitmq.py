from fastapi import FastAPI, HTTPException, Depends
from pydantic import BaseModel, EmailStr
import psycopg2
from psycopg2 import sql
import hashlib
from typing import Optional
import os
import redis
import pika
import json

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


DB_CONFIG = {
    "dbname": "play",
    "user": "play_user",
    "password": "test1234",
    "host": "localhost",
}

REDIS_CONFIG = {
    "DB": "0",
    "user": "play_user",
    "password": "test1234",
    "host": "localhost",
}

RABBITMQ_CONFIG = {
    "virtual_host": "play",
    "user": "play_user",
    "password": "test1234",
    "host": "localhost",
}


def get_db_connection():
    try:
        conn = psycopg2.connect(**DB_CONFIG)
        return conn
    except psycopg2.Error as e:
        print(f"Error connecting to the database: {e}")
        return None


def get_rabbitmq_connection():
    url_connection_string = (
        "amqp://"
        + RABBITMQ_CONFIG["user"]
        + ":"
        + RABBITMQ_CONFIG["password"]
        + "@"
        + RABBITMQ_CONFIG["host"]
        + ":"
        + "5672"
        + "/"
        + RABBITMQ_CONFIG["virtual_host"]
    )
    parameters = pika.URLParameters(url_connection_string)

    try:
        connection = pika.BlockingConnection(parameters=parameters)
        return connection
    except Exception as e:
        print(f"Error connecting to RabbitMQ: {e}")
        return False


def get_redis_connection():
    try:
        creds_provider = redis.UsernamePasswordCredentialProvider(
            REDIS_CONFIG["user"], REDIS_CONFIG["password"]
        )
        r = redis.StrictRedis(
            host=REDIS_CONFIG["host"],
            port=6379,
            db=REDIS_CONFIG["DB"],
            credential_provider=creds_provider,
            decode_responses=True,
        )
        return r
    except Exception as e:
        print(f"Error connecting to Redis: {e}")
        return False


def hash_password(password: str) -> str:
    return hashlib.sha256(password.encode()).hexdigest()


class User(BaseModel):
    username: str
    email: EmailStr
    password: str


def send_email_to_queue(connection, email, username):
    # Connect to RabbitMQ server
    channel = connection.channel()

    # Declare a queue
    queue_name = "email_queue"
    # channel.queue_declare(queue=queue_name, durable=True)
    email_details = {
        "to_email": email,
        "user_name": username,
        "subject": "Welcome to Our Service",
        "body": f"Dear {username},\n\nThank you for registering with our service!\n\nBest regards,\nThe Team",
    }
    # Publish the email details to the queue
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=json.dumps(email_details),
        properties=pika.BasicProperties(
            delivery_mode=2,  # Make message persistent
        ),
    )

    print(f"Sent email details to queue: {email_details}")


@app.post("/register")
def register(user: User):
    hashed_password = hash_password(user.password)

    conn = get_db_connection()
    r = get_redis_connection()
    connection = get_rabbitmq_connection()
    if conn is None:
        raise HTTPException(status_code=500, detail="Database connection error")

    if r is None:
        raise HTTPException(status_code=500, detail="Redis Server connection error")

    try:
        if user.username:
            username_exist = r.exists(user.username)
            if username_exist:
                error_message = "The username exits, try changing it"
        if user.email:
            email_exist = r.exists(user.email)
            if email_exist:
                error_message = "The email exits, try changing it"

        if username_exist and email_exist:
            error_message = "The user detail you specified already exits, try resetting your password"
            raise HTTPException(status_code=400, detail=error_message)
        elif username_exist or email_exist:
            raise HTTPException(status_code=400, detail=error_message)
        else:
            cur = conn.cursor()
            insert_query = sql.SQL(
                "INSERT INTO users (username, email, password) VALUES (%s, %s, %s)"
            )
            cur.execute(insert_query, (user.username, user.email, hashed_password))
            conn.commit()
            cur.close()
            r.set(user.username, 1)
            r.set(user.email, 1)
            send_email_to_queue(
                connection=connection, email=user.email, username=user.username
            )
    except psycopg2.IntegrityError:
        conn.rollback()
        raise HTTPException(status_code=400, detail="Username or email already exists")
    except Exception as e:
        conn.rollback()
        raise HTTPException(status_code=500, detail=f"An error occurred: {e}")
    finally:

        conn.close()

    return {"message": "User registered successfully"}
