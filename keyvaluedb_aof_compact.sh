#!/bin/bash
SEGMENT_SIZE=5
SEGMENT_NAME=segment
db_init () {
       touch segment0
}

db_set () {
	test -f "$SEGMENT_NAME0" || db_init
	current_segment_name=$(ls -lXr $SEGMENT_NAME* | awk '{print $9}' | tail -n 1)
	current_size=$(wc -l $current_segment_name | awk '{print $1}')
	if [ $current_size -lt $SEGMENT_SIZE ]; 
	then
		echo "$1,$2" >> $current_segment_name
	else 
		id=${current_segment_name/#$SEGMENT_NAME}
		id=$(($id+1))
		echo "$1,$2" >> "$SEGMENT_NAME$id"
	fi
}

db_get () {
	segment_list=$(ls -lXr $SEGMENT_NAME* | awk '{print $9}')
	for segment in $segment_list
	do
		grep "^$1," $segment | sed -e "s/^$1,//" | tail -n 1
		if [ ${PIPESTATUS[0]} == 0 ]
			then
				return 0
		fi 
	done
}


db_compact () {
	COMPACTION_FILENAME=compaction_segmentfile
	segment_list=$(ls -lXr $SEGMENT_NAME* | awk '{print $9}')
	for segment in $segment_list
	do
		sort -k1 -n $segment | uniq >> $COMPACTION_FILENAME
	done	
	#sort -k1 -n $COMPACTION_FILENAME | uniq > $COMPACTION_FILENAME
	#merged_segment_size=$(wc -l $COMPACTION_FILENAME | awk '{print $1}')
	keys=($(sed -E 's/(\w\d*)\,.*/\1/' $COMPACTION_FILENAME | sort |  uniq))
	merged_segment_size=$(sed -E 's/(\w\d*)\,.*/\1/' $COMPACTION_FILENAME | sort  | uniq | wc -l)
	compacted_segment_count=$(($merged_segment_size / $SEGMENT_SIZE))
	echo "Merged segment size is: $merged_segment_size"
	echo "Compacted segment count is $compacted_segment_count"
	for i in $(eval echo "{0..$compacted_segment_count}")
	do
		echo "The i value is: $i"
		offset=$i*$SEGMENT_SIZE
		for key in $(eval echo "${keys[@]:$offset:$SEGMENT_SIZE}")
		do
			echo "The current key value is: $key"
			grep $key $COMPACTION_FILENAME | head -1 >> "test_$SEGMENT_NAME$i"
		done
		
		#tail -n +$(($i*$SEGMENT_SIZE+1)) $COMPACTION_FILENAME | head -n $SEGMENT_SIZE > "test_$SEGMENT_NAME$i"
	done
}
