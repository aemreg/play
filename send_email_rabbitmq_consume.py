import pika
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

SMTP_CONFIG = {
    "smtp_server": "smtp.gmail.com",
    "smtp_port": 587,
    "from_email": "sampleplayemailaddress@gmail.com",
    "password": "nfrrchqzqjatqvte",
}

RABBITMQ_CONFIG = {
    "virtual_host": "play",
    "user": "play_user",
    "password": "test1234",
    "host": "localhost",
}


def get_rabbitmq_connection():
    url_connection_string = (
        "amqp://"
        + RABBITMQ_CONFIG["user"]
        + ":"
        + RABBITMQ_CONFIG["password"]
        + "@"
        + RABBITMQ_CONFIG["host"]
        + ":"
        + "5672"
        + "/"
        + RABBITMQ_CONFIG["virtual_host"]
    )
    parameters = pika.URLParameters(url_connection_string)

    try:
        connection = pika.BlockingConnection(parameters=parameters)
        return connection
    except Exception as e:
        print(f"Error connecting to Redis: {e}")
        return False


def send_registration_email(to_email, username, subject, body):
    # Gmail SMTP server address
    smtp_server = SMTP_CONFIG["smtp_server"]
    smtp_port = SMTP_CONFIG["smtp_port"]

    # Your Gmail credentials
    from_email = SMTP_CONFIG["from_email"]
    password = SMTP_CONFIG["password"]

    # Create the MIME message
    msg = MIMEMultipart()
    msg["From"] = from_email
    msg["To"] = to_email
    msg["Subject"] = subject

    msg.attach(MIMEText(body, "plain"))

    try:
        # Connect to the Gmail SMTP server and send email
        server = smtplib.SMTP(smtp_server, smtp_port)
        server.starttls()  # Secure the connection
        server.login(from_email, password)
        text = msg.as_string()
        server.sendmail(from_email, to_email, text)
        server.quit()
        print(f"Registration email sent to {to_email}")
    except Exception as e:
        print(f"Failed to send email: {e}")


def callback(ch, method, properties, body):
    email_details = json.loads(body)
    send_registration_email(
        to_email=email_details["to_email"],
        username=email_details["user_name"],
        subject=email_details["subject"],
        body=email_details["body"],
    )
    ch.basic_ack(delivery_tag=method.delivery_tag)


def consume_email_queue():
    # Connect to RabbitMQ server
    connection = get_rabbitmq_connection()
    channel = connection.channel()

    # Declare a queue
    queue_name = "email_queue"

    # Consume messages from the queue
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=queue_name, on_message_callback=callback)

    print("Waiting for messages. To exit press CTRL+C")
    channel.start_consuming()


if __name__ == "__main__":
    consume_email_queue()
