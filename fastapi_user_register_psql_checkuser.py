from fastapi import FastAPI, HTTPException, Depends
from pydantic import BaseModel, EmailStr
import psycopg2
from psycopg2 import sql
import hashlib
from typing import Optional
import os

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


DB_CONFIG = {
    "dbname": "play",
    "user": "play_user",
    "password": "test1234",
    "host": "localhost",
}


def get_db_connection():
    try:
        conn = psycopg2.connect(**DB_CONFIG)
        return conn
    except psycopg2.Error as e:
        print(f"Error connecting to the database: {e}")
        return None


def hash_password(password: str) -> str:
    return hashlib.sha256(password.encode()).hexdigest()


class User(BaseModel):
    username: str
    email: EmailStr
    password: str


@app.post("/register")
def register(user: User):
    hashed_password = hash_password(user.password)

    conn = get_db_connection()
    if conn is None:
        raise HTTPException(status_code=500, detail="Database connection error")

    try:
        cur = conn.cursor()
        if user.username:
            query = sql.SQL("SELECT EXISTS (SELECT 1 FROM users WHERE username = %s)")
            cur.execute(query, (user.username,))
            username_exist = cur.fetchone()[0]
            if username_exist:
                error_message = "The username exits, try changing it"

        if user.email:
            query = sql.SQL("SELECT EXISTS (SELECT 1 FROM users WHERE email = %s)")
            cur.execute(query, (user.email,))
            email_exist = cur.fetchone()[0]
            if email_exist:
                error_message = "The email exits, try changing it"

        if username_exist and email_exist:
            error_message = "The user detail you specified already exits, try resetting your password"
            raise HTTPException(status_code=400, detail=error_message)
        elif username_exist or email_exist:
            raise HTTPException(status_code=400, detail=error_message)
        else:
            insert_query = sql.SQL(
                "INSERT INTO users (username, email, password) VALUES (%s, %s, %s)"
            )
            cur.execute(insert_query, (user.username, user.email, hashed_password))
            conn.commit()
    except psycopg2.IntegrityError:
        conn.rollback()
        raise HTTPException(status_code=400, detail="Username or email already exists")
    except Exception as e:
        conn.rollback()
        raise HTTPException(status_code=500, detail=f"An error occurred: {e}")
    finally:
        cur.close()
        conn.close()

    return {"message": "User registered successfully"}
