#!/bin/bash

db_set () {
    echo "$1,$2" >> "segment"
}

db_get () {

    grep "^$1," segment | sed -e "s/^$1,//" | tail -n 1
}
