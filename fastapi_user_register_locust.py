from locust import HttpUser, task, between
import random

gmail_adresses = ["ksplatformservisleritechradar@gmail.com", "aliemregursu@gmail.com"]


def add_dot_to_username(username, indices):
    username_list = list(username)
    for idx, index in enumerate(indices):
        if idx > 0:
            username_list.insert(index + idx, ".")
        else:
            username_list.insert(index, ".")

    return "".join(username_list)


class UserRegistrationTest(HttpUser):
    wait_time = between(1, 3)

    def on_start(self):
        random_email = random.choice(gmail_adresses)
        username = random_email.split("@")[0]
        length = len(username) - 1
        dots_to_add = random.randrange(1, length)
        dot_places = random.sample(range(1, length), dots_to_add)
        dot_places.sort()
        new_username = add_dot_to_username(username=username, indices=dot_places)
        self.username = new_username

    @task
    def register_user(self):
        headers = {"Content-Type": "application/json"}
        payload = {
            "username": self.username,
            "email": self.username + "@gmail.com",
            "password": "test1234",
        }
        response = self.client.post("/register", json=payload, headers=headers)
        if response.status_code == 200:
            print("User registered successfully!")
        else:
            print(f"Failed to register user: {response.text}")
