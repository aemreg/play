#!/bin/bash
SEGMENT_SIZE=5
SEGMENT_NAME=segment
db_init () {
       touch segment0
}

db_set () {
	test -f "$SEGMENT_NAME0" || db_init
	current_segment_name=$(ls -ltr $SEGMENT_NAME* | awk '{print $9}' | tail -n 1)
	current_size=$(wc -l $current_segment_name | awk '{print $1}')
	if [ $current_size -lt $SEGMENT_SIZE ]; 
	then
		echo "$1,$2" >> $current_segment_name
	else 
		id=${current_segment_name/#$SEGMENT_NAME}
		id=$(($id+1))
		echo "$1,$2" >> "$SEGMENT_NAME$id"
	fi
}

db_get () {
	segment_list=$(ls -lt $SEGMENT_NAME* | awk '{print $9}')
	for segment in $segment_list
	do
		grep "^$1," $segment | sed -e "s/^$1,//" | tail -n 1
		if [ ${PIPESTATUS[0]} == 0 ]
			then
				return 0
		fi 
	done
}
